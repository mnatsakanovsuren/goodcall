$(document).ready(() => {
    const ACTIVE = "active",
            BODY = $('body');

    class CommonElements{

        constructor(){

            svg4everybody();

            var count = $('.count');
            var loader = $('.preloader');
            $({ Counter: 0 }).animate({ Counter: 100 }, {
                duration: 3000,
                step: function () {
                    count.text(Math.ceil(this.Counter)+ "%");
                },
                complete: function () {
                    setTimeout(function() {
                        loader.addClass('none');
                        BODY.removeClass(ACTIVE);
                    }, 300);
                }
            });

            $('#select-items').niceSelect();


            BODY.on({
                focus: function () {
                    $(this).parent('label').addClass('focus');
                    $(this).parent().removeClass('error');
                },
                blur: function () {
                    // $(this).parent().removeClass('focus');
                    if($(this).val().length == 0) {
                        $(this).parent('label').removeClass('focus');
                    }
                },
                input: function () {
                    // $(this).parent().removeClass('focus');
                    if($(this).val().length > 0){
                        $(this).parent('label').addClass('focus');
                    } else {
                        $(this).parent('label').removeClass('focus');
                    }
                }
            }, '.field-js');


            $(function() {
                var positions = [],
                    currentActive = null,
                    links = $('.side-menu>a');

                $(".info-block").each(function(){
                    positions.push({
                        top: $(this).position().top + 400,
                        a: links.filter('[href="#'+$(this).attr('id')+'"]')
                    });
                });

                positions = positions.reverse();

                $(window).on('scroll',function() {
                    var winTop = $(window).scrollTop();
                    for(var i = 0; i < positions.length; i++){
                        if(positions[i].top < winTop){
                            if(currentActive !== i){
                                currentActive = i;
                                links.removeClass('active');
                                positions[i].a.addClass("active");
                            }
                            break;
                        }
                    }
                });
            });

            // $('.preloader').fadeOut(2500, function () {});

            $('.menu').click(function() {
               $('.main-menu').toggleClass(ACTIVE);
               $('.menu-burger').toggleClass(ACTIVE);
               BODY.toggleClass(ACTIVE);
               $('.header').toggleClass('all-white');
               $('header a').toggleClass('menu-open');
               $('.footer').toggleClass('menu-open');
               $('.logo-color').toggleClass('hidden');
            });

            $('.request-form-active').click(function() {
                $('.request-form').addClass(ACTIVE);
                BODY.addClass(ACTIVE);
                $('.close-popup-form').click(function () {
                    $('.request-form').removeClass(ACTIVE);
                    BODY.removeClass(ACTIVE);
                });

            });

            $('.open-text').click(function(){
                $('.main-paragraph>.inner').toggleClass('opened');
                $(this).toggleClass('opened');
            });


            //Main menu
            if(window.matchMedia('(max-width: 767px)').matches) {


                $('.header .location').insertAfter($('.main-menu ul'));
                $('.header .mail').insertAfter($('.main-menu ul'));
                $('.header .tel').insertAfter($('.main-menu ul'));
                $('.menu-footer .languages').insertBefore('.main-menu ul');

            }

            $('.scroll-top').click(function(){
                $('html,body').animate({scrollTop: 0}, 1000);
            });

            $(".side-menu").on("click","a", function (event) {
                event.preventDefault();

                let id  = $(this).attr('href'),
                    top = $(id).offset().top-80;


                $('.side-menu a').removeClass(ACTIVE);
                $(this).addClass(ACTIVE);


                //анимируем переход на расстояние - top за 1500 мс
                $('body,html').animate({scrollTop: top}, 700);
            });

            // $(window).scroll(function () {
            //     let height = document.documentElement.scrollTop;
            // });

            $('.popup-js').fancybox({
                padding: 0,
                btnTpl: {
                    close: $(this).find('.close-popup')
                }
            });

            $(document).on('click', '.popup .close-popup', function() {
                $.fancybox.close();
            });

            $('.fancybox').fancybox({
                openEffect  : 'none',
                closeEffect : 'none',
                maxWidth: 800,
                maxHeight: 600,
                btnTpl : {
                    close: '<div data-fancybox-close ' +
                                'class="fancybox-button fancybox-button--close" ' +
                                'title="{{CLOSE}}">' +
                                '<svg>' +
                                    '<use xlink:href="./img/sprite-inline.svg#ic_close"></use>' +
                                '</svg>' +
                            '</div>'

                }
            });

            // Arrow for sliders
            let cardSliderArrowRight = '<div class="slider-arrow arrow-right"><svg>\n' +
                '  <circle id=\'shape\' cx=\'50%\' cy=\'50%\' r=\'49%\' />\n' +
                '</svg></div>';
            let cardSliderArrowLeft = '<div class="slider-arrow arrow-left"></div>';

            $('.slider').slick({
                infinite: true,
                speed: 500,
                slidesToShow: 1,
                slidesToScroll: 1,
                dots: true,
                nextArrow: cardSliderArrowRight,
                prevArrow: cardSliderArrowLeft,
                // autoplay: true,
                autoplaySpeed: 4000,
                pauseOnHover: false
            });

            $('.slider').on('init', function(event, slick, currentSlide, nextSlide){
                $('#shape').addClass('animate');
            });

            $('.slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                $('#shape').removeClass('animate');
            });
            $('.slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
                $('#shape').addClass('animate');
            });

            $('.reviews-slider').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                nextArrow: cardSliderArrowRight,
                prevArrow: cardSliderArrowLeft,
                // autoplay: true,
                autoplaySpeed: 4000,
                asNavFor: '.reviews-slider-nav',
                pauseOnHover: false
            });

            $('.reviews-slider').on('init', function(event, slick, currentSlide, nextSlide){
                $('.reviews-slider #shape').addClass('animate');
            });

            $('.reviews-slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                $('.reviews-slider #shape').removeClass('animate');
            });
            $('.reviews-slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
                $('.reviews-slider #shape').addClass('animate');
            });

            $('.reviews-slider-nav').slick({
                slidesToShow: 4,
                slidesToScroll: 1,
                asNavFor: '.reviews-slider',
                focusOnSelect: true,
                variableWidth: true
            });


            $('.tabs>ul>li>a').click(function(event){
                event.preventDefault();

                $('.tabs>ul>li>a').removeClass();
                $(this).addClass(ACTIVE);

                let idTab = $(''+$(this).attr('href'));
                $('.tabs-content>div').removeClass(ACTIVE).fadeOut(0);
                $(idTab).fadeIn(500);
            });


            $('.custom-scroll').mCustomScrollbar({
                axis: 'x',
                contentTouchScroll: true,
                documentTouchScroll: true,
                autoExpandHorizontalScroll:true,
                advanced: {
                    extraDraggableSelectors: '.custom-scroll'
                },
                scrollButtons: {
                    enable: true
                },
                callbacks:{
                    onInit: function(){
                        $('.mCSB_buttonRight').append(cardSliderArrowRight);
                        $('.mCSB_buttonLeft').append(cardSliderArrowLeft);
                        $('.mCSB_buttonRight').appendTo($('.advantages .wrapper'));
                        $('.mCSB_buttonLeft').appendTo($('.advantages .wrapper'));

                    }
                }
            });

            if(window.matchMedia('(min-width: 992px)').matches) {
                $('.vertical-scroll').mCustomScrollbar({
                    axis: 'y'
                });
            }


        }


    }
    new CommonElements();

    if (BODY.hasClass('index')) {
        $('.to-top-slide').click(function(){
            $.fn.pagepiling.moveTo(1);
        });

        if(window.matchMedia('(min-width: 768px)').matches) {
            $('#pagepiling').pagepiling({
                sectionSelector: 'section',
                css3: true,
                // touchSensitivity: 1000,
                afterLoad: function (index) {
                    let leavingSection = $(this);

                    leavingSection.addClass('visited');


                    if (leavingSection.hasClass('main-section')) {

                        $('.slide-count ul li').removeClass(ACTIVE);
                        $('.slide-count ul li:first-child').addClass(ACTIVE);
                    }


                },
                afterRender: function(anchorLink, index){
                    let leavingSection = $(this);

                    $('.show-scroll').addClass('animate');

                    if (leavingSection.hasClass('main-section')) {
                        $('.header').addClass('main-section__menu');
                        $('.footer').addClass('main-section__footer');
                        $('.slide-count ul li:first-child').addClass(ACTIVE);
                    }

                    if (leavingSection.hasClass('geography')) {
                        $('.geography__image').addClass('animate');
                    }

                },
                onLeave: function (index, nextIndex) {
                    $('.slide-count ul li').removeClass(ACTIVE);

                    if (nextIndex === 1) {
                        $('.slide-count ul li').removeClass(ACTIVE);
                        $('.slide-count ul li:first-child').addClass(ACTIVE);
                        $('.header').removeClass('service-header');
                    }

                    if (nextIndex === 2) {
                        $('.header').removeClass('main-section__menu');
                        $('.header').addClass('service-header');
                        $('.footer').removeClass('main-section__footer');

                        $('.slide-count ul li:nth-child(2)').addClass(ACTIVE);

                    } else if ( nextIndex !== 2 ) {
                        $('.header').addClass('main-section__menu');
                        $('.footer').addClass('main-section__footer');
                        $('.header').removeClass('service-header');
                    }
                    if (nextIndex === 3) {
                        $('.header').addClass('need-header');
                        $('.footer').addClass('need-footer');
                        $('.header').removeClass('main-section__menu');
                        $('.footer').removeClass('main-section__footer');
                        $('.slide-count ul li').removeClass(ACTIVE);
                        $('.slide-count ul li:nth-child(3)').addClass(ACTIVE);
                        // $('.logo svg>use').attr('xlink:href','img/sprite-inline.svg#logo')

                    } else if ( nextIndex !== 3  ) {
                        $('.header').removeClass('need-header');
                        $('.footer').removeClass('need-footer');
                        // $('.logo svg>use').attr('xlink:href','img/sprite-inline.svg#logo-white')
                    }
                    if (nextIndex === 4) {
                        $('.header').addClass('adv-header');
                        $('.footer').addClass('adv-footer');
                        $('.header').removeClass('main-section__menu');
                        $('.footer').removeClass('main-section__footer');
                        $('.slide-count ul li:nth-child(4)').addClass(ACTIVE);
                        // $('.logo svg>use').attr('xlink:href','img/sprite-inline.svg#logo')

                    } else if ( nextIndex !== 4  ) {
                        $('.header').removeClass('adv-header');
                        $('.footer').removeClass('adv-footer');

                    }


                    if (nextIndex === 5) {
                        $('.header').addClass('reviews-header');
                        $('.footer').addClass('reviews-footer');
                        $('.header').removeClass('main-section__menu');
                        $('.footer').removeClass('main-section__footer');
                        $('.slide-count ul li:nth-child(5)').addClass(ACTIVE);
                        // $('.logo svg>use').attr('xlink:href','/wp-content/themes/goodcall/img/sprite-inline.svg#logo')
                    } else if (nextIndex !== 5) {
                        $('.header').removeClass('reviews-header');
                        $('.footer').removeClass('reviews-footer');
                    }
                    if (nextIndex === 6) {
                        $('.header').addClass('geography-header');
                        $('.footer').addClass('geography-footer');
                        $('.header').removeClass('main-section__menu');
                        $('.footer').removeClass('main-section__footer');
                        $('.slide-count ul li:nth-child(6)').addClass(ACTIVE);
                        // $('.logo svg>use').attr('xlink:href','/wp-content/themes/goodcall/img/sprite-inline.svg#logo')
                    } else if (nextIndex !== 6) {
                        $('.header').removeClass('geography-header');
                        $('.footer').removeClass('geography-footer');
                    }



                    $('.slide-count>p').text('0' + nextIndex);

                }

            });
        }



        if(window.matchMedia('(max-width: 991px)').matches) {

            $('.geography__wrapper .main-btn').insertAfter($('.point.location-3'));


        }
    }
    if (BODY.hasClass('blog')) {

        if(window.matchMedia('(max-width: 991px)').matches) {

            $('.blog__article h5').insertAfter($('.blog-main--left h1'));
            $('.blog__article .date').insertAfter($('.blog-main--left h1'));


        }

    }
    if ($('html').hasClass('messenger')) {
        console.log($('.next-service').offset().top-$('.next-service').height());
        $(window).scroll(function(){
            let height = window.pageYOffset,
                block = $('.sidemenu-wrapper').offset().top-80;
            if (height > block) {
                $('.side-menu').addClass('fixed');
                if (height > $('.next-service').offset().top-$('.next-service').height()+70) {
                    $('.side-menu').addClass('stop');
                } else {
                    $('.side-menu').removeClass('stop');
                }
            } else {
                $('.side-menu').removeClass('fixed');
            }
        });
    }
    if ($('html').hasClass('outgoing')) {
        $(window).scroll(function(){
            let height = window.pageYOffset,
                block = $('.sidemenu-wrapper').offset().top-80;
            if (height > block) {
                $('.side-menu').addClass('fixed');
                if (height > $('.next-service').offset().top-$('.next-service').height()-300) {
                    $('.side-menu').addClass('stop');
                } else {
                    $('.side-menu').removeClass('stop');
                }
            } else {
                $('.side-menu').removeClass('fixed');
            }
        });
    }
    if ($('html').hasClass('incoming')) {
        $(window).scroll(function(){
            let height = window.pageYOffset,
                block = $('.sidemenu-wrapper').offset().top-80;
            if (height > block) {
                $('.side-menu').addClass('fixed');
                if (height > $('.next-service').offset().top-$('.next-service').height()-200) {
                    $('.side-menu').addClass('stop');
                } else {
                    $('.side-menu').removeClass('stop');
                }
            } else {
                $('.side-menu').removeClass('fixed');
            }

            $('.animate-image').each(function(){
                let flag = true;
                let block = $(this),
                    blockHeight = block.offset().top-500;

                $(window).scroll(function() {
                    let height = document.documentElement.scrollTop;

                    if (height > blockHeight && flag) {
                        flag = false;
                        block.addClass('animated right');
                    }

                });

            });

        });
    }
    if ($('html').hasClass('about')) {
        setTimeout(function () {
            $('.animate-image').first().addClass('animated right')
        }, 3400);

        $(window).scroll(function(){
            let height = window.pageYOffset,
                block = $('.sidemenu-wrapper').offset().top-80;
            if (height > block) {
                $('.side-menu').addClass('fixed');
                if (height > $('.work').offset().top-$('.work').height()+210) {
                    $('.side-menu').addClass('stop');
                } else {
                    $('.side-menu').removeClass('stop');
                }
            } else {
                $('.side-menu').removeClass('fixed');
            }

            $('.animate-image').each(function(){
                let flag = true;
                let block = $(this),
                    blockHeight = block.offset().top-500;

                $(window).scroll(function() {
                    let height = window.pageYOffset;

                    if (height > blockHeight && flag) {
                        flag = false;
                        block.addClass('animated right');
                    }

                });

            });

        });
    }

    if (BODY.hasClass('contact')) {
        if(window.matchMedia('(min-width: 992px)').matches) {
            $('.contact-link').click(function (e) {
                e.preventDefault();
                $('html').animate({scrollTop: 1000}, 700);
            })
        }
        // $(".contact-link").mouseenter(function (e) {
        //     e.stopPropagation();
        //     $('#eye img').attr("src", "img/pages/elems/icon-hireus.gif");
        // });
        // $(".contact-link").mouseleave(function (e) {
        //     e.stopPropagation();
        //     $('#eye img').attr("src", "img/pages/elems/icon-hireus.png");
        // });
    }
    if (BODY.hasClass('careers')) {
        // $(".careers-offers__image").mouseenter(function (e) {
        //     e.stopPropagation();
        //     $('#eye img').attr("src", "img/pages/elems/icon-hireus.gif");
        // });
        // $(".careers-offers__image").mouseleave(function (e) {
        //     e.stopPropagation();
        //     $('#eye img').attr("src", "img/pages/elems/icon-hireus.png");
        // });

        setTimeout(function () {
            $('.animate-image').first().addClass('animated right')
        }, 3300);
        if(window.matchMedia('(min-width: 992px)').matches) {
            $('.careers-offers__image').click(function (e) {
                e.preventDefault();
                $('html').animate({pageYOffset: 1500}, 700);
            })
        }
        $(window).scroll(function () {
            $('.animate-image').each(function(){
                let flag = true;
                let block = $(this),
                    blockHeight = block.offset().top-500;

                $(window).scroll(function() {
                    let height = document.documentElement.scrollTop;

                    if (height > blockHeight && flag) {
                        flag = false;
                        block.addClass('animated right');
                    }

                });

            });
        });
    }

    if (BODY.hasClass('careers-all') || $('html').hasClass('vacancie')) {
        setTimeout(function () {
            $('.animate-image').first().addClass('animated right')
        }, 3300);
    }

    // if ($('html').hasClass('cases')) {
    //      $(".cases__info--row.short>.image").mouseenter(function (e) {
    //         e.stopPropagation();
    //         $('#eye img').attr("src", "img/pages/elems/icon-hireus.gif");
    //     });
    //     $(".cases__info--row.short>.image").mouseleave(function (e) {
    //         e.stopPropagation();
    //         $('#eye img').attr("src", "img/pages/elems/icon-hireus.png");
    //     });
    // }

});
